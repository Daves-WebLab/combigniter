<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *-----------------------------------------------------
 * SLIDE PUSH MENU
 *-----------------------------------------------------
 *
 * necessary, intializes the side push menu with entries
 * in CB_Controller
 * 
 */
$config['cb_spmenu'] = array(
    'cb_spmenu_dashboard' => 'comboard/dashboard',
    'cb_spmenu_test' => 'comboard/test'
);