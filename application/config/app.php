<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| ajax_views
|--------------------------------------------------------------------------
|
| this array defines which views are loadable via ajax using app.load.view().
| other views that might need user credentials (rights, ...) should use
| conventional ajax using app.ajax or app.json on a custom controller
|
*/
$config['ajax_views'] = array();

/*
|--------------------------------------------------------------------------
| default_lang
|--------------------------------------------------------------------------
|
| used by class/LanguageLoader to define which language to use by default
|
*/
$config['default_lang'] = 'en';