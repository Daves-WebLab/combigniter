<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AJAX_Controller extends CI_Controller {
    // the ajax request object
    protected $ajax;
    // the language loader object
    protected $lang_loader;
    // the view loader object
    protected $view_loader;
    
    public function __construct($use_ajax = true) {
        parent::__construct();
        
        // disable boilerplate by default
        // as AJAX usually does not load complete pages
        $this->boilerplate->disable();
        
        if(!$this->input->is_ajax_request()) {
            show_error('sorry, but you do not belong here! this URL is used for AJAX Requests only');
        } else if($use_ajax) {
            // initialize language loader
            include_class('app/LanguageLoader');
            $this->lang_loader =& LanguageLoader::get_instance();
            
            // initialize view loader
            include_class('app/ViewLoader');
            $this->view_loader = new ViewLoader();
            
            // initialize ajax request
            include_class('app/AJAX_Request');
            $this->ajax = new AJAX_Request();
        }
    }
    
    /**
     *
     * once the requested controller function is done
     * execute this function, basically renders the ajax output
     * back to the browser
     *
     */
    public function _output($output) {
        // send response to client
        echo $this->ajax->render();
    }
}
