<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appClass implements Iterator {
    private $data;
    
    private $position;
    
    public function __construct($data=array()) {
        if(is_object($data) || is_array($data)) {
            $this->data = $data;
        } else {
            $this->data = array();
        }
        $position = 0;
    }
    
    public function __set($name, $value) {
        $this->data[$name] = $value;
    }
    
    public function __get($name) {
        if(array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return false;
    }
    
    public function add($data) {
        if(is_object($data) || is_array($data)) {
            foreach($data as $key => &$value) {
                $this->data[$key] = $value;
            }
        }
    }
    
    public function to_array() {
        return $this->data;
    }
    
    public function current() {
        $val = $this->by_index($this->position);
        return $val['val'];
    }
    
    public function key() {
        $val = $this->by_index($this->position);
        return $val['key'];
    }
    
    public function next() {
        ++$this->position;
    }
    
    public function rewind() {
        $this->position = 0;
    }
    
    public function valid() {
        return $this->position >= 0 && $this->position < count($this->data);
    }
    
    private function by_index($index) {
        $i = 0;
        foreach($this->data as $key => &$val) {
            if($i == $index) {
                return array(
                    'key' => $key,
                    'val' => $val
                );
            }
        }
    }
}