<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * this class can handle CodeIgniters language loading
 * it decides from which language to load a language file based on a session key
 * the default language to use can be set in config/app
 *
 * @author David Riedl <david.riedl@codecomb.eu>
 *
 */
class LanguageLoader {
    private static $INSTANCE;
    
    public static function &get_instance() {
        if(self::$INSTANCE !== null) {
            return self::$INSTANCE;
        } else {
            self::$INSTANCE = new self;
            return self::$INSTANCE;
        }
    }
    
    // name of the session key
    private static $SESSION_KEY = 'LanguageLoader_lang';
    // CodeIgniter instance
    private $ci;
    
    // currently active language
    private $lang;
    
    private function __construct() {
        // get CI instance
        $this->ci =& get_instance();
        
        // load session library
        $this->ci->load->library('session');
        
        // load app config
        $this->ci->load->config('app');
        
        // if language has not been set in the session till now
        if(($this->lang = $this->ci->session->userdata(self::$SESSION_KEY)) === false) {
            // set the default language defined in config/app
            $this->lang = $this->ci->config->item('default_lang');
            // set the language in the session
            $this->ci->session->userdata(self::$SESSION_KEY, $this->lang);
        }
    }
    
    private function __clone() {}
    
    /**
     *
     * change to another language
     *
     * @param string $lang
     *      the language to change to (see folder structure in application/language)
     *
     */
    public function change($lang) {
        $this->lang = $lang;
        
        $this->ci->session->set_userdata(self::$SESSION_KEY, $this->lang);
    }
    
    /**
     *
     * get the currently active language
     *
     * @return string
     *      the language currently active
     *
     */
    public function lang() {
        return $this->lang;
    }
    
    /**
     *
     * load one or multiple language files using CodeIgniters lang->load()
     * but handles the decision which language to load for you
     *
     * @param misc $lang
     *      relative path to one language file like default CI->lang->load()
     *      or an array of relative paths
     *
     */
    public function load($lang) {
        if(is_array($lang)) {
            foreach($lang as &$file) {
                $this->ci->lang->load($file, $this->lang);
            }
        } else {
            $this->ci->lang->load($lang, $this->lang);
        }
    }
    
    /**
     *
     * translate one or multiple language keys from the loaded language files
     *
     * @param misc $key
     *      either one key or an array of keys
     *
     * @return one string if key was a string, associative array with $key => $value pairs otherwise
     *
     */
    public function translate($key) {
        if(is_array($key)) {
            // if $key is an array translate each key and place it in an assoc array
            $lang = array();
            
            foreach($key as &$k) {
                $lang[$k] = $this->ci->lang->line($k);
                if($lang[$k] === false) {
                    $lang[$k] = 'L::'.$k;
                }
            }
            
            return $lang;
        } else {
            // else simply translate this one key
            $lang = $this->ci->lang->line($key);
            if($lang === false) {
                return 'L::'.$key;
            }
            return $lang;
        }
    }
}

/**
 *
 * simplifies translation usage to a simple function translate
 * see LanguageLoader::translate() for usage
 *
 */
function translate($key) {
    return LanguageLoader::get_instance()->translate($key);
}