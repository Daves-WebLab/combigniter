<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class appLog {
    private static $INSTANCE;
    private static $log;
    
    private function __construct() {
        self::$log = fopen(FCPATH . APPPATH .'logs/app.log', 'a+');
    }
    
    private function __clone() {}
    
    public static function &get_instance() {
        if(self::$INSTANCE === null) {
            self::$INSTANCE = new self();
        }
        return self::$INSTANCE;
    }
    
    public static function log($message) {
        if(self::$log) {
            $timestamp = date('Y-m-d | G:i:s');
            fwrite(self::$log, '['. $timestamp .'] '. $message . PHP_EOL);
            fflush(self::$log);
        }
    }
}
