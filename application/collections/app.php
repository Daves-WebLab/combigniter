<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * default app, which the Minifier trys to load on construct (can be removed if not wanted)
 *
 */
$collection['app'] = array(
    'js/vendor/app.js'
);