<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Load extends AJAX_Controller {    
    public function __construct() {
        parent::__construct();
        
        // load app config and app language file
        $this->load->config('app');
        $this->lang_loader->load('app');
    }
    
    public function view() {
        // only execute if view is set and is a view which can be loaded via ajax
        if(($view = $this->ajax->data('view')) && in_array($view, $this->config->item('ajax_views'))) {
            // if language files are needed load them
            if(($lang = $this->ajax->data('lang'))) {
                $this->lang_loader->load($lang);
            }
            
            // get the data for the view
            $data = $this->ajax->data('data');
            
            // load the view with stringify on and place output inside the ajax request
            $this->ajax->add($this->view_loader->view($view)->add($data)->load(true));
        } else {
            // something went wrong
            $this->ajax->error($this->lang_loader->translate('app_error_load_view'), 0);
        }
    }
    
    public function lang() {
        // get wanted language file(s) and key(s)
        if(($files = $this->ajax->data('files')) && ($keys = $this->ajax->data('keys'))) {
            // load language file(s)
            $this->lang_loader->load($files);
            
            // translate language key(s)
            if(is_array($keys)) {
                $this->ajax->add($this->lang_loader->translate($keys));
            } else {
                $this->ajax->add($keys, $this->lang_loader->translate($keys));
            }
        } else {
            $this->ajax->error($this->lang_loader->translate('app_error_load_lang'),0);
        }
    }
}