<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_class('app/appClass');

if(!function_exists('app')) {
    function app($data=false) {
        return new appClass($data);
    }
}

if(!function_exists('applog')) {
    function applog($message) {
        appLog::log($message);
    }
}