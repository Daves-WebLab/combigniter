<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * this class simplifies view loading, by making data setting easier
 *
 * @author David Riedl <david.riedl@codecomb.eu>
 *
 */
class ViewLoader {
    // CI instance
    private $ci;

    // templates to load
    private $templates;
    
    // the view to load
    private $view;
    // data for the view
    private $data;
    
    public function __construct() {
        // get CI instance
        $this->ci =& get_instance();
        
        // initialize default values
        $this->templates = array();
        $this->view = '';
        $this->data = array();
    }
    
    public function &template($template, $view_key) {
        $this->templates[] = array(
            'template' => $template,
            'view_key' => $view_key
        );
        
        return $this;
    }
    
    /**
     *
     * get or set the view based on the passed parameter
     *
     * @parameter string $view default null
     *      if null this function returns the current view
     *      otherwise the current view is set to $view (like ci->load->view())
     *
     * @return misc
     *      if $view == null it returns the current view
     *
     */
    public function &view($view=null) {
        if($view === null) {
            return $this->view;
        } else {
            $this->view = $view;
        }
        return $this;
    }
    
    /**
     *
     * get previously set data by key
     *
     * @param string $key
     *      key of the previously set data element
     *
     * @return misc
     *      value if the key was valid, false otherwise
     *
     */
    public function data($key) {
        if(isset($this->data[$key])) {
            return $this->data[$key];
        }
        return false;
    }
    
    /**
     *
     * add data to the view loader
     *
     * @param misc $misc
     *      key for $val, or an array
     *
     * @param misc $val
     *      value behind $misc,
     *      if $misc was an array the $key => $value pairs of it will be stored
     *      and $val is ignored
     *
     */
    public function &add($misc, $val=null) {
        if($misc !== false) {
            if(is_array($misc)) {
                foreach($misc as $key => &$value) {
                    $this->data[$key] = $value;
                }
            } else {
                $this->data[$misc] = $val;
            }
        }
        return $this;
    }
    
    /**
     *
     * remove a data element by key
     *
     * @param misc key
     *      key to be removed from data
     *
     */
    public function &remove($key) {
        if(isset($this->data[$key])) {
            unset($this->data[$key]);
        }
        return $this;
    }
    
    /**
     *
     * load the view with CI's load function, while recursivly building
     * the added templates
     *
     * @param boolean $stringify default false
     *      if true the view is returned as string
     *
     */
    public function load($stringify=false) {
        $len = count($this->templates);
        
        if($len === 0) {
            return $this->ci->load->view($this->view, $this->data, $stringify);
        } else {
            $template_content = '';
            for($i=$len-1; $i>=0; $i--) {
                // get current template
                $template =& $this->templates[$i];
                
                if($i === $len-1) {
                    if($this->view === '') {
                        $view_content = '';
                    } else {
                        // load view if it is the first step
                        $view_content = $this->ci->load->view($this->view, $this->data, true);
                        // place the view_content as data
                    }
                    $this->add($template['view_key'], $view_content);
                } else {
                    // if it is another step place the template_content as data
                    $this->add($template['view_key'], $template_content);
                }
                
                if($i !== 0) {
                    // recursive place templates into each other
                    $template_content = $this->ci->load->view($template['template'], $this->data, true);
                } else {
                    // last step, place complete produced data in the last template
                    return $this->ci->load->view($template['template'], $this->data, $stringify);
                }
            }
        }
    }
}