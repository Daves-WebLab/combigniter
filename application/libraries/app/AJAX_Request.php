<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * this class can be used together with app.js --> app.ajax, app.json
 * to simplify your ajax experience
 *
 * it handles request type checking and data type checking, stores the
 * return value and is able to render the return value based on the data type
 *
 * @author David Riedl <david.riedl@codecomb.eu>
 *
 */
class AJAX_Request {
    // reference to the CodeIgniter instance
    private $ci;
    
    // all request headers
    private $headers;
    
    // type of the request (must be get or post for utmost funcionality)
    private $request_type = false;
    
    // type of the request (either json or text)
    private $data_type = false;
    
    private $return_value;
    
    public function __construct() {
        // get CodeIgniter instance
        $this->ci =& get_instance();
        
        // get all request headers
        $this->headers = $this->ci->input->request_headers();
        
        // get the request type, using the REQUEST_METHOD server-variable
        if(($method = $this->ci->input->server('REQUEST_METHOD')) !== false) {
            $this->request_type = strtolower($method);
        } else {
            $this->request_type = false;
        }
        
        // check if accept type is json or text (two most important)
        if(preg_match('/application\/(.*)json/', $this->headers['Accept']) == 1) {
            $this->ci->output->set_header('Content-Type: application/json; charset=utf-8');
            $this->data_type = 'json';
            $this->return_value = array(
                'error' => false,
                'err' => array(
                    'or' => '',
                    'no' => -1
                )
            );
        } else {
            $this->ci->output->set_header('Content-Type: text/html; charset=utf-8');
            $this->data_type = 'text';
            $this->return_value = '';
        }
    }
    
    /**
     *
     * get a received value by key, only works if the request_type was get or post
     *
     * @param string $key
     *      key of the variable
     *
     * @return mixin
     *      value behind the key, false if key or request_type invalid
     *
     */
    public function data($key) {
        // only if valid request type
        if($this->request_type) {
            // get the input variable by request_type, returns false if request_type is wrong
            return call_user_func_array(array($this->ci->input, $this->request_type), array($key));
        }
        return false;
    }
    
    /**
     *
     * get a previously added value by key if type is json, the previously added text/html otherwise
     *
     * @param string $key
     *      key of the variable if type is json
     *
     * @return mixin
     *      value behind key if json, text/html otherwise
     *
     */
    public function get($key=null) {
        switch($this->data_type) {
            case 'json':
                if($key !== null && isset($this->return_value[$key])) {
                    return $this->return_value[$key];
                }
                return false;
            
            case 'text':
                return $this->return_value;
        }
    }
    
    /**
     *
     * add content to the response
     *
     * @param misc $misc
     *      either key for $val if type is json, text/html otherwise
     *
     * @param misc $val
     *      value behind $misc if type is json, left out otherwise
     *
     */
    public function add($misc, $val = null) {
        if($misc !== false) {
            switch($this->data_type) {
                case 'json':
                    $this->set_json($misc, $val);
                    break;
                    
                case 'text':
                    $this->set_text($misc, $val);
                    break;
            }
        }
    }
    
    private function set_json($misc, $val) {
        if(is_array($misc)) {
            foreach($misc as $key => &$value) {
                $this->return_value[$key] = $value;
            }
        } else {
            $this->return_value[$misc] = $val;
        }
    }
    
    private function set_text($text, $override) {
        if($override !== null && $override === true) {
            $this->return_value = $text;
        } else {
            $this->return_value .= $text;
        }
    }
    
    /**
     *
     * set error for the ajax response
     *
     * @param string $error
     *      usually a textual representation of the error
     *
     * @param mixin $errno
     *      usually a error number
     *
     */
    public function error($error, $errno) {        
        if(!is_array($this->return_value)) {
            $text = $this->return_value;
            $this->return_value = array();
            $this->return_value['text'] = $text;
        } 
        
        $this->return_value['error'] = true;
        $this->return_value['err'] = array(
            'or' => $error,
            'no' => $errno
        );
    }
    
    /**
     *
     * render the ajax response
     *
     * @return mixin
     *      json string if type was json, pure text/html otherwise
     *
     */
    public function render() {
        if(is_array($this->return_value) && isset($this->return_value['error']) && $this->return_value['error'] === true) {
            return json_encode($this->return_value);
        }
        
        switch($this->data_type) {
            case 'json':
                return json_encode($this->return_value);
            
            case 'text':
                return $this->return_value;
        }
    }
    
    /**
     *
     * get the request type of the ajax call
     *
     * @return string
     *      the request type
     *
     */
    public function request_type() {
        return $this->request_type;
    }
    
    /**
     *
     * get the ajax request's data type
     *
     * @return string
     *      the data type
     *
     */
    public function data_type() {
        return $this->data_type;
    }
}