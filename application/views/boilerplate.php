<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- iOS and Android -->
        <link rel="apple-touch-icon" href="<?= base_url() . RELATIVE_ASSETSPATH .'img/favicon/apple-touch-icon-57x57.png' ?>"/>
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() . RELATIVE_ASSETSPATH .'img/favicon/apple-touch-icon-76x76.png' ?>" />
        
        <!-- default with .ico fallback -->
        <link rel="icon" type="image/png" href="<?= base_url() . RELATIVE_ASSETSPATH .'img/favicon/favicon-32x32.png' ?>" />
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() . RELATIVE_ASSETSPATH .'img/favicon/favicon-ico.ico' ?>" />
        
        <!-- custom meta tags -->
        <?= $meta ?>
        
        <!-- custom linked css files -->
        <?= $linked_css ?>
        
        <!-- custom internal style -->
        <?= $inline_css ?>
    </head>
    <body>
        <!-- Page Content -->
        <!-- ------------ -->
        
        <?= $content ?>
        
        <!-- ------------- -->
        <!-- /Page Content -->
        
        <!-- best practice jQuery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?= base_url() . RELATIVE_ASSETSPATH .'js/vendor/jquery.min.js' ?>"><\/script>')</script>
        
        <!-- custom js files -->
        <?= $linked_js ?>
        <!-- custom internal scripts -->
        <?= $inline_js ?>
    </body>
</html>
<!-- thank you for using CombIgniter -->