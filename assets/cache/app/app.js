var app = (function($) {
    // loader class on the client-side
    function Loader() {
        this.load_path = '<?= $app_path ?>load/';
    }
    
    Loader.prototype.view = function($options) {
        var options = {
            view: false,
            lang: false,
            data: false,
            callback: function() {}
        }
        
        $.extend(options, $options);
        
        app.ajax({
            url: this.load_path + 'view',
            data: {
                view: options.view,
                lang: options.lang,
                data: options.data
            }, success: function(e, response) {
                if (e.error) {
                    notification.show({type: 'error', message: e.err.or + ', <strong>view = ' + view + '</strong>'});
                } else if (options.callback) {
                    options.callback(response);
                }
            }
        });
    }
    
    Loader.prototype.lang = function($options) {
        var options = {
            files: false,
            keys: false,
            callback: function() {}
        }
        
        $.extend(options, $options);
        
        app.json({
            url: this.load_path + 'lang',
            data: {
                files: options.files,
                keys: options.keys
            }, success: function(e, response) {
                if (e.error) {
                    notification.show({type: 'error', message: e.err.or});
                } else if (options.callback) {
                    options.callback(response);
                }
            }
        });
    }
    
    var load = new Loader();
    
    // set basic url with php (adapt to https etc.)
    var base_url = '<?= $base_url ?>';
    
    /**
     *
     * post an ajax call to the server
     *
     * @param Object options
     *      adapted from jquery's ajax
     *          - url = default is comb.base_url and options.url is appended to that
     *          - success will be overriden and always returns a json object + the server response
     *            so it has to be a function(e, response)
     *
     *            where e is a json object with
     *              - error (false if no error, true if error)
     *              - err {
     *                   or: actual error
     *                   no: error number
     *                }
     *            and response is the actual server response
     * 
     */
    function ajax(options) {
        if (options.url != undefined) {
            options.url = base_url + options.url;
        }
        
        o = {
            url: base_url
        }
        
        $.extend(o, options);
        
        var callback = false;
        if (options.success != undefined) {
            callback = options.success;
        }
        
        o.success = function(response) {
            if (response.error == undefined) {
                try {
                    json = $.parseJSON(response);
                } catch(err) {
                    json = {
                        error: false,
                        err: {
                            or: '',
                            no: -1
                        }
                    }
                }
            } else {
                json = {
                    error: response.error,
                    err: {
                        or: response.err.or,
                        no: response.err.no
                    }
                }
                
                delete response.error;
                delete response.err;
            }
            
            if (callback != false) {
                callback(json, response);
            }
        }
        
        return $.ajax(o);
    }
    
    
    /**
     *
     * same as ajax, only that default type = 'post' and dataType = 'json'
     *
     */
    function json(options) {
        o = {
            type: 'post',
            dataType: 'json'
        }
        
        $.extend(o, options);
        return ajax(o);
    }
    
    function NotificationManager() {
        var notifications = [];
        
        function show(options) {
            options.onClose = function(notification) {
                // splicing resets all indizes → do same with IDs!
                for (var i=notification.id+1; i<notifications.length; i++) {
                    notifications[i].id -= 1;
                }
                
                var height = notification.notification.outerHeight();
                
                for(var i=0; i<notification.id; i++) {
                    notifications[i].margin -= height + 10;
                    notifications[i].notification.css('margin-bottom', notifications[i].margin + 'px');
                }
                
                notifications.splice(notification.id, 1);
            }
            
            var notification = new Notification(notifications.length, options);
            
            var height = notification.notification.outerHeight();
            
            // move all other notifications up!
            for (var i=0; i<notifications.length; i++) {
                notifications[i].margin += height + 10;
                notifications[i].notification.css('margin-bottom', notifications[i].margin + 'px');
            }
            
            notifications[notifications.length] = notification;
        }
        
        return {
            show: show
        }
    }
    
    function Notification($id, $options) {
        var $this = this;
        
        this.id = $id;
        this.margin = 0;
        
        var o = {
            type: 'standard',
            message: '',
            timeout: false,
            colors: {
                error: {
                    border: '#e34f4f',
                    background: '#e76a69',
                    text: '#ffffff',
                }, warning: {
                    border: '#d18b2a',
                    background: '#e8ab56',
                    text: '#ffffff',
                }, success: {
                    border: '#80ad53',
                    background: '#B0D080',
                    text: '#37363B',
                }, standard: {
                    border: '#1a9dd9',
                    background: '#33afe8',
                    text: '#ffffff',
                }
            }, css: {
                'position': 'fixed',
                'z-index': '10000',
                'bottom': '8%',
                'left': '50%',
                'padding': '10px 30px 10px 10px',
                'font-size': '14px',
                'border': '1px solid',
                '-ms-filter': '"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"',
                'filter': 'alpha(opacity=0)',
                '-moz-opacity': '0',
                '-webkit-opacity': '0',
                '-khtml-opacity': '0',
                'opacity': '0',
                '-moz-box-shadow': '0px 0px 1px -1px black',
                '-webkit-box-shadow': '0px 0px 1px -1px black',
                'box-shadow' : '0px 0px 1px -1px black',
                'moz-transition': 'margin 300ms ease',
                'webkit-transition': 'margin 300ms ease',
                'transition': 'margin 300ms ease',
            }, closebutton: {
                css: {
                    'cursor': 'pointer',
                    'position': 'absolute',
                    'right': '5px',
                    'top': '10px',
                    'padding': '0 5px',
                    '-ms-filter': '"progid:DXImageTransform.Microsoft.Alpha(Opacity=40)"',
                    'filter': 'alpha(opacity=40)',
                    '-moz-opacity': '0.4',
                    '-webkit-opacity': '0.4',
                    '-khtml-opacity': '0.4',
                    'opacity': '0.4'
                }, hover: {
                    '-ms-filter': '"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)"',
                    'filter': 'alpha(opacity=100)',
                    '-moz-opacity': '1',
                    '-webkit-opacity': '1',
                    '-khtml-opacity': '1',
                    'opacity': '1'
                }
            }, onClose: function(id) {}
        }
        
        $.extend(o, $options);
        
        this.onClose = o.onClose;
        
        if (o.colors[o.type] != undefined) {
            o.css['background'] = o.colors[o.type].background;
            o.css['border-color'] = o.colors[o.type].border;
            o.css['color'] = o.colors[o.type].text;
        }
        
        this.notification = $('<div></div>');
        this.notification.css(o.css);
        this.notification.html(o.message);
        
        $('body').append(this.notification);
        
        var closebutton = $('<div>&#x2715;</div>');
        closebutton.css(o.closebutton.css);
        
        closebutton.hover(function() {
            closebutton.css(o.closebutton.hover);
        }, function() {
            closebutton.css(o.closebutton.css);
        });
        
        closebutton.on('click', function(event) {
            $this.close();
        });
        
        if (o.timeout) {
            window.setTimeout(function() {
                $this.close();
            }, o.timeout);
        }
        
        this.notification.css('margin-left', '-' + this.notification.outerWidth()/2 + 'px');
        this.notification.css({
            'display': 'none',
            '-ms-filter': '"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)"',
            'filter': 'alpha(opacity=100)',
            '-moz-opacity': '1',
            '-webkit-opacity': '1',
            '-khtml-opacity': '1',
            'opacity': '1'
        });
        
        this.notification.append(closebutton);
        
        this.notification.fadeIn();
        
        return this;
    }
    
    Notification.prototype.close = function() {
        var $this = this;
        this.notification.fadeOut(function() {
            $this.onClose($this);
            $this.notification.remove()
        });
    }
    
    var notification = new NotificationManager();
    
    return {
        base_url: base_url,
        ajax: ajax,
        json: json,
        load: load,
        notification: notification
    }
})(jQuery);