<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 *---------------------------------------------------------------------
 * Meta Class
 *---------------------------------------------------------------------
 *
 * this class demonstrates usefullness by simpliness. All it does is
 * stack meta tags and concatenate them when calling render on it.
 *
 * @author David Riedl <david.riedl@codecomb.eu>
 * 
 */
class CI_Meta {
    // array storing all meta tags
    private $meta = array();
    
    /**
     *
     * simply add a new meta tag
     * 
     * @param string $content
     *      the meta tag
     *      
     */
    public function add($content) {
        $this->meta[] = $content;
    }
    
    /**
     *
     * concatenate all meta tags
     *
     * @return string
     *      the concatenated meta tags
     *
     */
    public function render() {
        $content = '';
        foreach($this->meta as $meta) {
            $content .= $meta;
        }
        return $content;
    }
}