<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This class is used to enable the Boilerplate default template, or disable it
 * if not needed in given controllers
 * 
 */
class CI_Boilerplate {
    private $enabled;
    
    public function __construct() {
        $this->enabled = TRUE;
    }
    
    /**
     *
     * check if boilerplate is enabled
     *
     * @return boolean
     *
     */
    public function is_enabled() {
        return $this->enabled;
    }
    
    /**
     *
     * disable boilerplate
     *
     */
    public function disable() {
        $this->enabled = FALSE;
    }
    
    /**
     *
     * enable boilerplate
     *
     */
    public function enable() {
        $this->enabled = TRUE;
    }
}