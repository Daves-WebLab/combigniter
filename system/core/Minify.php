<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CI_Minify {
    // all supported asset types that can be added
    private $types;
    
    // the current swapped IDs (file names)
    private $id;
    
    // maps types to compiler
    private $compiler;
    
    // maps types to minifier
    private $minifier;
    
    // if minify is wannted or not
    private $minify;
    
    // added assets
    private $min;
    
    // asset wrappers
    private $tags;
    
    private $loaded_collections = array();
    
    // all collections stored in config/collections.php
    private $collections = array();
    
    public function __construct() {
        // set up types
        $this->types = array(
            'css' => 'css',
            'scss' => 'css',
            'js' => 'js'
        );
        
        // set up IDs
        $this->id = array(
            'css' => array(
                'id' => false,
                'ignore' => false
            ), 'js' => array(
                'id' => false,
                'ignore' => false
            )
        );
        
        // map compilers
        $this->compiler = array(
            'scss' => 'scssc'
        );
        
        // map minifiers
        $this->minifier = array(
            'css' => 'cssmin',
            'js' => 'jsmin'
        );
        
        // check if minifying is wanted
        $this->minify = load_class('Config', 'core')->config['minify_assets'];
        
        // set up asset storage
        $this->min = array(
            'css' => array(
                'linked' => array(),
                'inline' => array()
            ),
            'js' => array(
                'linked' => array(),
                'inline' => array()
            )
        );
        
        // set up asset wrappers
        $this->tags = array(
            'css' => array(
                'linked' => '<link rel="stylesheet" href="{content}">',
                'inline' => '<style>{content}</style>'
            ),
            'js' => array(
                'linked' => '<script type="text/javascript" src="{content}"></script>',
                'inline' => '<script>{content}</script>'
            )
        );
        
        // CombIgniters app-collection
        $this->load_collections('app', true);
        $this->add_collection('app', true);
    }
    
    /**
     *
     * swap to a new id, which is a relative path without extension to the file
     * where all afterwards added assets will be compiled and minified to
     * 
     * @param string $id
     *      the id to swap to (relative file path from assets directory, extension is important! only 'css' and 'js' are valid)
     *
     * @param boolean $ignore
     *      if true this file will not be rebuild even if force is set when minifying
     *      (useful for big scss files which do not have to be compiled all the time, bootstrap.scss e.g.)
     *
     */
    public function swap($id, $ignore=false) {
        $info = pathinfo($id);
        if(isset($info['extension'])) {
            $type = $info['extension'];
           
            // check if $type is valid
            if($this->is_valid_swaptype($type)) {
                // add the new ID
                $this->id[$type]['id'] = 'cache/'. $id;
                // set if this ID is being ignored from rebuilding
                $this->id[$type]['ignore'] = $ignore;
                return true;
            }
        }
        
        return false;
    }
    
    /**
     *
     * add a new asset to the minifier
     *
     * @param string $type
     *      add a linked asset (e.g. <link href="...css"/>):
     *          $type is the relative path to the file from the assets directory ('css/vendor/bootstrap.scss' e.g.)
     *      add a inline asset (e.g. <style>...</style>):
     *          $type has to be css, scss or js
     *
     * @param $content
     *      add a linked asset: not needed
     *      add a inline asset:
     *          $content is the actual content that will be added inline
     *
     */
    public function add($type, $content=false) {
        // if $content = false is has to be a linked asset and $type is the actual content
        if($content === false) {
            $content = $type;
            $type = false;
        }
        
        // get the assets category (if it is a file path -> linked, else inline)
        $category = $this->get_category($content);
        if($category === 'linked') {
            // if it is linked get the paths extension
            $info = pathinfo($content);
            $type = $info['extension'];
            
            // check the validity of the type
            if($this->is_valid_type($type)) {
                // get the array to add the asset to
                $assets =& $this->min[$this->get_type($type)][$category][$this->id[$this->get_type($type)]['id']];
                
                // if this asset already exist ignore it
                if(isset($assets) && isset($assets[$content])) { 
                    return;
                } else {
                    // else set the assets ignore based on the current ID
                    $assets['ignore'] = $this->id[$this->get_type($type)]['ignore'];
                }
                
                // add the asset and its type
                $assets[$content] = $type;
            }
        } else if($category === 'inline') {
            // if inline just add it
            $this->min[$this->get_type($type)][$category][] = array(
                'type' => $type,
                'content' => $content
            );
        }
    }
    
    public function load_collections($path, $noerror = false) {
        if(in_array($path, $this->loaded_collections)) return;
        
        $c_file = FCPATH . APPPATH . 'collections/' . $path . EXT;
        
        if(file_exists($c_file)) {
            $this->loaded_collections[] = $path;
            
            $collection = array();
            include($c_file);
            
            foreach($collection as $name => &$assets) {
                $info = explode('|', $name);
                $name = $info[0];
                
                if(count($info) > 1 && $info[1] == 'ignore') {
                    $ignore = true;
                } else {
                    $ignore = false;
                }
                
                $this->collections[$name] = array(
                    'file' => $path,
                    'assets' => $assets,
                    'ignore' => $ignore
                );
            }
        } else if(!$noerror) {
            log_message('error', 'invalid collections file: '. $c_file);
        }
    }
    
    /**
     *
     * add a collection of assets to the minifier (seperate files)
     *
     * @param string $name
     *      name of the collection (it's relative path defined in config/collections.php)
     *
     * @param boolean $noerror
     *      if set to true no error will be logged if file does not exists
     *
     */
    public function add_collection($name, $noerror=false) {
        // check if it is a valid collection
        if(isset($this->collections[$name])) {
            // get the collection
            $collection =& $this->collections[$name];
                
            // get the type of collection
            foreach($this->id as $type => $oldswap) {
                $swap_name = $collection['file'] .'/'. $name .'.'. $type;
                if($this->swap($swap_name, $collection['ignore'])) {
                    foreach($collection['assets'] as &$asset) {
                        $info = pathinfo($asset);
                        $asset_type = $info['extension'];
                        
                        if(isset($this->types[$asset_type]) && $this->types[$asset_type] == $type) {
                            $this->add($asset);
                        }
                    }
                    
                    $this->id[$type] = $oldswap;
                } 
            }
        } else if(!$noerror) {
            log_message('error', 'invalid collection name: '. $name);
        }
    }
    
    /**
     *
     * minify the added assets, this will produce a file for each swapped ID (to which assets where actually added)
     *
     * @param string $type
     *      type of asset that will be compiled and minified, has to be 'css' or 'js'
     *
     * @param string $category
     *      category which assets will be compiled and minified, has to be 'linked' or 'inline'
     *
     * @param boolean $force
     *      default false, if true the asset will be rebuild everytime (except the assets ID was swapped with ignore on)
     *
     * @return string
     *      if $category was 'linked':
     *          the <link> or <script> tags with the files href's or src's (amount depends on amount of swap calls with unique IDs)
     *      if $category was 'inline':
     *          the <style> or <script> tag with the compiled and minified content in it
     *
     */
    public function minify($type, $category, $force=false) {
        // this will either be the actual return output
        $output = array(); 
        
        // if the $type and $category are valid
        if($this->is_valid_type($type) && $this->is_valid_category($category)) {
            // get the wanted assets by reference
            $assets =& $this->min[$type][$category];
            // and setup up the needed Minifier, and load it
            $minifier = $this->minifier[$type];
            require_once(BASEPATH . 'core/minifier/' . $minifier .'.php');
            
            // if no assets have been added ignore it
            if(count($assets) > 0 && $category === 'linked') {
                // for each swapped to ID
                foreach($assets as $id => &$id_assets) {
                    // check if this ID has to be ignored or not
                    $ignore = $id_assets['ignore'];
                    unset($id_assets['ignore']);
                    
                    // create the actual file path
                    $file = ASSETSPATH . $id;
                    
                    // get the files directory path
                    $dir = dirname($file);
                    // if it does not exist create all needed directories
                    if(!file_exists($dir) && $dir !== '.') {
                        mkdir($dir, 0755, true);
                    }
                    
                    // if file does not exist or force is activated and it is not being ignored
                    if(!file_exists($file) || ($force && !$ignore)) {
                        // open the handle
                        $handle = fopen($file, 'w');
                        
                        // if fopen worked
                        if($handle !== false) {
                            // setup file content and content
                            $file_content = '';
                            $content = '';
                            
                            // the needed compiler (only one type allowed)
                            $compiler = false;
                            
                            // files which are already minified (based on the files name ...min...)
                            $non_minified = array();
                            // files which don't need to be compiled (based on the extension, currently only scss has to be compiled)
                            $non_compiled = array();
                            $i = 0;
                            $j = 0;
                            
                            foreach($id_assets as $asset => &$asset_type) {
                                // check if the asset exists
                                if(file_exists(ASSETSPATH . $asset)) {
                                    // if the file needs to be compiled and no compiler is set yet
                                    if(!$compiler && $this->needs_compilation($asset_type)) {
                                        // load the needed compiler
                                        require_once(BASEPATH . 'core/compiler/' . $this->compiler[$asset_type] .'.php');
                                        $compiler = new $this->compiler[$asset_type]();
                                        // set the import path
                                        $compiler->setImportPaths(ASSETSPATH);
                                    }
                                    
                                    // get the files content
                                    $content = file_get_contents(ASSETSPATH . $asset);
                                    
                                    // place a comment if it does not need to be minified or compiled
                                    if($this->minify && preg_match('/\.min\./', $asset) === 1) {
                                        $non_minified[$i] = $content;
                                        $content = '/*|m'. $i++ .'|*/';
                                    } else if(!$this->needs_compilation($asset_type)) {
                                        $non_compiled[$j] = $content;
                                        $content = '/*|c'. $j++ .'|*/';
                                    }
                                    
                                    $file_content .= $content;
                                }
                            }
                            
                            // compile the needed content
                            if($compiler !== false) {
                                $file_content = $compiler->compile($file_content);
                            }
                            
                            // fix scssc greatness of placing comments inside css blocks
                            $matched = array();
                            while(preg_match('/\/\*\*\|([c|m][0-9]+)\|\*\/ *}/', $file_content, $matched) === 1) {
                                $file_content = str_replace($matched[0], '}/*|'.$matched[1].'|*/', $file_content);
                            }
                            
                            // replace comment tags to something the minfier doesnt kill
                            foreach($non_minified as $key => $content) {
                                $file_content = str_replace('/*|m'. $key .'|*/', '.cssmin_dont_minify_this_here{margin:'. $key .'}', $file_content);
                            }
                            
                            // place the assets which need to be minified back in
                            foreach($non_compiled as $key => $content) {
                                $file_content = str_replace('/*|c'. $key .'|*/', $content, $file_content);
                            }
                            
                            /* minify */
                            if($this->minify) {
                               $file_content = $minifier::minify($file_content);
                            }
                            
                            // replace the already minified assets back in
                            foreach($non_minified as $key => $content) {
                                $file_content = str_replace('.cssmin_dont_minify_this_here{margin:'. $key .'}', $content, $file_content);
                            }
                            
                            // write content, flush and close the handle
                            fwrite($handle, $file_content);
                            fflush($handle);
                            fclose($handle);
                        }
                    }
                    
                    // for this id create a output entry
                    $output[] = base_url() . RELATIVE_ASSETSPATH . $id;
                }
            } else if(count($assets) > 0 && $category === 'inline') {
                $content = '';
                $compiler = false;
                
                // inline assets
                foreach($assets as $asset) {
                    $content .= $asset['content'];
                    
                    // check if it needs to be compiled and set the compiler like above
                    if(!$compiler && $this->needs_compilation($asset['type'])) {
                        require_once(BASEPATH . 'core/compiler/' . $this->compiler[$asset['type']] .'.php');
                        $compiler = new $this->compiler[$asset['type']]();
                        $compiler->setImportPaths(ASSETSPATH);
                    }
                }
                
                // if it needs to be compiled do so
                if($compiler !== false) {
                    $content = $compiler->compile($content);
                }
                
                // minify it if needed
                if($this->minify) {
                    $content = $minifier::minify($content);
                }
                
                // create a output entry
                $output[] = $content;
            }
        }
        
        // check the type and place the created output inside the wanted tags and return them
        $return = '';
        foreach($output as $o) {
            $return .= str_replace('{content}', $o, $this->tags[$type][$category]);
        }
        
        return $return;
    }
    
    /*
     *
     *-----------------------------------------------------------------------------------------
     * HELPER FUNCTIONS
     *-----------------------------------------------------------------------------------------
     *
     */
    
    private function is_valid_type($type) {
        return isset($this->types[$type]);
    }
    
    private function is_valid_swaptype($type) {
        return isset($this->id[$type]);
    }
    
    private function is_valid_category($category) {
        return $category === 'linked' || $category === 'inline';
    }
    
    private function get_type($type) {
        return $this->types[$type];
    }
    
    private function needs_compilation($type) {
        return isset($this->compiler[$type]);
    }
    
    private function get_category($content) {
        return preg_match('/^[a-zA-Z0-9\.\-\_\/]+\.[a-zA-Z]{2,4}$/', $content) ? 'linked' : 'inline';
    }
}

/*
 *
 * file ends here
 * system/core/Minify.php
 * @author David Riedl <david.riedl@codecomb.eu>
 *
 */