<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('server_variable')) {
    function server_variable($var) {
        if(isset($_SERVER[$var])) {
            return $_SERVER[$var];
        }
        return false;
    }
}

if(!function_exists('include_path')) {
    function include_path($path, $return=false) {
        if(file_exists(FCPATH . APPPATH . $path . EXT)) {
            if($return) {
                return include(FCPATH . APPPATH . $path . EXT);
            } else {
                include_once(FCPATH . APPPATH . $path . EXT);
                return true;
            }
        }
        
        return false;
    }
}

if(!function_exists('include_class')) {
    function include_class($relative_path) {
        include_path('class/' . $relative_path);
    }
}

if(!function_exists('get_variable')) {
    function get_variable($relative_path) {
        return include_path('variables/' . $relative_path, true);
    }
}

if(!function_exists('get_controller')) {
    function get_controller($controller) {
        if(include_path('controllers/' . $controller)) {
            $controller = ucfirst(basename($controller));
            
            return new $controller();
        }
    }
}

/*
 *---------------------------------------------------------------
 * PREFIX LOGIC
 *---------------------------------------------------------------
 *
 * this logic will get the current server prefix if this page is in a subfolder
 * 
 * by CombIgniter
 * @author David Riedl <david.riedl@codecomb.eu>
 * 
 */
        
$prefix = '';
// get the current folders absolute path without trailing slash
$current_folder = rtrim(str_replace('\\', '/', FCPATH), '/');
// get the server root folder's absolute path
$document_root = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);

// if they are not the same it has to be a subfolder
if($document_root !== $current_folder) {
    // explode the request uri which has the prefix
    $parts = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
    $parts_count = count($parts);
    
    $found = false;
    
    // now try parts combined logically until the folders match --> actual prefix
    for($i=0; $i<$parts_count && !$found; $i++) {
        if($i == 0) {
            $prefix .= $parts[$i];
        } else {
            $prefix .= '/'. $parts[$i];
        }
        
        // it has to be realpath --> soft-links?
        if(str_replace('\\', '/', realpath($document_root.'/'.$prefix)) == $current_folder) {
            $found = true;
        }
    }
    
    // if it could not be found there is something strange and maybe you want to debug this logic
    if(!$found) {
        exit('sorry something went wrong getting server prefix, index.php');
    }
    
    // define the prefix
    define('DOCUMENT_PREFIX', $prefix);
} else {
	define('DOCUMENT_PREFIX', '');
}

/*
 *---------------------------------------------------------------
 * ASSET ACCESS
 *---------------------------------------------------------------
 *
 * now check if it is a asset request and actualy pointing to the asset
 * folder to prevent blocking a controller function called assets.
 * else start CodeIgniter
 *
 * by CombIgniter
 * @author David Riedl <david.riedl@codecomb.eu>
 *
 */
if (preg_match('/\/?'. str_replace('/', '\/', DOCUMENT_PREFIX) .'\/'. $assets_folder .'\/?/', $_SERVER['REQUEST_URI']) == 1) {
    require_once(BASEPATH . 'core/Common.php');
    
    /**
    *
    * get a wanted asset and cache it on the client side
    *
    * @param string $request
    *      the request url
    *
    * @return boolean/string
    *      false if not found or not modified,
    *      content of the file otherwise
    *
    * @author David Riedl <david.riedl@codecomb.eu>
    */
    if(!function_exists('cache_asset')) {
        function cache_asset($request) {
            if($request && preg_match('/(.*)?\/?'. trim(RELATIVE_ASSETSPATH, '/') .'\/?/', $request, $matched) == 1) {
                $relative_path = str_replace($matched[0], '', $request);
                $do_it = false;
                $is_ext = '';
                
                // if the wanted asset exists try to load it
                if(file_exists(ASSETSPATH . $relative_path)) {
                    $do_it = true;
                } else if(file_exists(ASSETSPATH . $relative_path . EXT)) {
                    $do_it = true;
                    $is_ext = EXT;
                }
                
                if($do_it) {    
                    // load the default config
                    $CFG =& load_class('Config', 'core');
                    
                    // create the absolute file path and get its info
                    $file = ASSETSPATH . $relative_path;
                    $file_info = pathinfo($file);
                    
                    // if it was a valid file
                    if(isset($file_info['extension'])) {
                        // if it actually is a EXT file
                        $file .= $is_ext;
                        
                        // check if last modified and etag are supported
                        $r_last_modified = server_variable('HTTP_IF_MODIFIED_SINCE');
                        $r_etag = server_variable('HTTP_IF_NONE_MATCH');
                        
                        // get the last time the file was modified and create an etag for it
                        $last_modified = @filemtime($file);
                        $etag = md5($last_modified . $file);
                        
                        if($r_last_modified !== false && (@strtotime($r_last_modified) >= $last_modified || $r_etag == $etag)) {
                            // if it was not modified send "not modified" header
                            header('HTTP/1.1 304 Not Modified');
                            return false;
                        } else {
                            // set all needed headers
                            $mime = $CFG->config['asset_mimes'][$file_info['extension']];
                            header('Expires: '. gmdate('D, d M Y H:i:s', (time() + (30 * 24 * 60 * 60))) . ' GMT');
                            header('Content-Type: '. $mime['mime'] .'; charset=utf-8');
                            header('Vary: Accept-Encoding');
                            header('Last-Modified: '. gmdate('D, d M Y H:i:s', $last_modified) . ' GMT');
                            header('Etag: "'. $etag .'"');
                        }
                        
                        // if zlib is available
                        if(extension_loaded("zlib") && !$mime['passthru']) {
                            // check if browser supports gzip or deflate
                            if(substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
                                header('Content-Encoding: gzip');
                                $compress = 'gzencode';
                            } else if(substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'deflate')) {
                                header('Content-Encoding: deflate');
                                $compress = 'gzdeflate';
                            }
                        } else {
                            $compress = false;
                        }
                        
                        ob_start();
                        if(preg_match('/'. str_replace('/', '\/', $CFG->config['app.js']) .'/', $file) == 1) {
                            if (isset($_SERVER['HTTP_HOST'])) {
                                $base_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
                                $base_url .= '://'. $_SERVER['HTTP_HOST'];
                                $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
                            } else {
                                $base_url = 'http://localhost/';
                            }
                            
                            $app_path = $CFG->config['app_path'];
                            
                            include($file);
                            
                        } else {
                            readfile($file);
                        }
                        
                        $output = ob_get_clean();
                        
                        // compress it or not
                        if($compress !== false && !$mime['passthru']) {
                            return call_user_func($compress, $output);
                        } else {
                            return $output;
                        }
                    }
                } else {
                    // if file does not exist send not found header
                    header('HTTP/1.0 404 Not Found');
                    return false;
                }
            }
            
            return false;
        }
    }
    
    /*
     * --------------------------------------------------------------------
     * LOAD THE ASSETS FILE
     * --------------------------------------------------------------------
     *
     * return assets, compress and cache them on the client side
     *
     */
    $asset = cache_asset(server_variable('REQUEST_URI'));
    if($asset) {
        echo $asset;
    }
} else {
    /*
     * --------------------------------------------------------------------
     * LOAD THE BOOTSTRAP FILE
     * --------------------------------------------------------------------
     *
     * And away we go...
     *
     */
    require_once BASEPATH.'core/CodeIgniter.php';
}